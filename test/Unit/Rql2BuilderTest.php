<?php declare(strict_types=1);

use Chronext\Rql2Dbal\Rql2Builder;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\DBAL\Query\QueryBuilder;
use PHPUnit\Framework\TestCase;

final class Rql2BuilderTest extends TestCase
{
    public function testReturnAll(): void
    {
        $qb  = $this->callRql2Builder('', ['field1', 'field2']);
        $sql = $qb->getSQL();

        $this->assertSame('SELECT field1, field2 FROM table_name', $sql);
    }

    public function testOneCondition(): void
    {
        $qb  = $this->callRql2Builder('ge(delivery%5Fdate,2015-06-02T20:00:00Z)', ['field1', 'delivery_date']);
        $sql = $qb->getSQL();

        $bindName = array_key_first($qb->getParameters());

        $this->assertSame('SELECT field1, delivery_date FROM table_name WHERE delivery_date >= :'.$bindName, $sql);
    }

    public function testWithOrCondition(): void
    {
        $qb  = $this->callRql2Builder('or(ge(field1,5),eq(field2,string:23))', ['field1', 'field2']);
        $sql = $qb->getSQL();

        $values = array_values($qb->getParameters());
        $this->assertSame(5, $values[0]);
        $this->assertSame('23', $values[1]);
        $bindNames = array_keys($qb->getParameters());

        $this->assertSame(
            "SELECT field1, field2 FROM table_name WHERE (field1 >= :${bindNames[0]}) OR (field2 = :${bindNames[1]})",
            $sql
        );
    }

    private function callRql2Builder(string $queryString, array $availableFields): QueryBuilder
    {
        $rql2Builder = new Rql2Builder();
        $qb          = $this->createQueryBuilder();
        $rql2Builder($qb, $queryString, $availableFields);

        return $qb;
    }

    private function createQueryBuilder(): QueryBuilder
    {
        $qb = new QueryBuilder(
            new class extends Connection
            {
                public function __construct()
                {
                    $this->_expr = new ExpressionBuilder($this);
                }
            }
        );

        $qb->from('table_name');

        return $qb;
    }

}
